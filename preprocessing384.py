from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
from nltk.tokenize import  TreebankWordTokenizer
from HTMLParser import HTMLParser
import nltk, re, sys


nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')


# initalizing word tokenizer and modifying one of its regex so that multiple punctuations
# such as !!! or ??? are not split
tbwt = TreebankWordTokenizer()
tbwt.PUNCTUATION[5] = (re.compile(r'[?!]+'), r' \g<0> ') # do not separate ? and !, ...

# initalizing sentence tokenizer, including a list of common abbreviations which should not
# generate a new sentence.
punkt_param = PunktParameters()
punkt_param.abbrev_types = set(['dr', 'vs', 'mr', 'mrs', 'prof', 'inc', 'e.g', 'i.e'])

sentence_splitter = PunktSentenceTokenizer(punkt_param)



class TwitterParser(HTMLParser):
	""" Given a csv string this class normalizes the data, detects sentences in the tweet,
		and tags each word with its type (e.g. noun, verb etc...)

		Class takes one argument, raw_tweet_data which is one line of data of the form 
		"tweet polarity, id, date of tweet, query, tweet user, tweet text" otherwise raises 
		an exception.
	"""
	def __init__(self, raw_tweet_data):
		self.reset()
		self.tweet_data = [] # normalized list of raw_tweet_data
		self.tweet = '' # tweet text string
		self.tweet_sentence = [] # a list of strings, each one a sentence of the tweet
		self.inside_HTML_element = 0; # used as a flag to ignor content inside HTML elements

		tweet = raw_tweet_data.split(',', 5)

		if len(tweet) < 5:
			raise Exception('Could not split ' + raw_tweet_data)

		for i in range(0,6):
			tweet[i] = tweet[i].replace('"', '')
			
			if i < 2:
				tweet[i] = int(tweet[i])
			else:
				tweet[i] = str(tweet[i])

		self.tweet_data = tweet

		self.feed(tweet[5]) # feed method is part of HTMLParser, removes all HTML
		
		self.url_special_character_removal()
		self.tweet_sentence = self.sentence_detection_and_word_tagger()


	def add_to_tweet(self, words):
		""" Append words to tweet text. 

				words - whole or part of a tweet text
		"""
		if self.inside_HTML_element == 0:
			self.tweet = self.tweet + words


	def handle_charref(self, num):
		""" Process HTML ASCII codes to their actual representation. If not found, 
			entire HTML ASCII code is removed from the tweet.

				num - the number part of the html ASCII, such as 35 in &#35;
		"""
		try:
			self.add_to_tweet( chr(int(num)) )
		except:
			return;


	def handle_data(self, words):
		""" Save word lists given by HTMLParser to tweet text (HTML has been removed)

				words - a list of strings containing original text from tweet minus any HTLM
		"""
		self.add_to_tweet( ''.join(words) )


	def handle_starttag(self, tag, attrs):
		""" Called by HTMLParser to signal an HTML element is being expanded. Tracks our current
			HTML element depth.
		"""
		self.inside_HTML_element += 1


	def handle_endtag(self, tag):
		""" Called by HTMLParser to signal an HTML element is being closed. Tracks our current
			HTML element depth.
		"""
		self.inside_HTML_element -= 1


	def url_special_character_removal(self):
		""" Processes tweet string so that all links starting with http(s) or www are removed.
			Also removes the symbols @ and #.
		"""
		self.tweet = re.sub(r"http\S+", "", self.tweet)
		self.tweet = re.sub(r"https\S+", "", self.tweet)
		self.tweet = re.sub(r"www\S+", "", self.tweet)
		self.tweet = re.sub(r"@\S+", "", self.tweet)
		self.tweet = re.sub(r"#\S+", "", self.tweet)

	
	def sentence_detection_and_word_tagger(self):
		""" Splits tweet string into sentences, then tags each word
			in the sentence with its word type (i.e. noun, verb etc...)

			returns a list of sentences which in turn contain a list of word strings, where each
			word string is "word/word_type" i.e. "dog/NN" (NN shorthand for noun)
		"""
		self.tweet = self.tweet + '*' # prevents end of line ellipses to be split since * is not an end of line character
									  # so it will be split instead. (Workaround)
		token_tweet = []
		for sentence in sentence_splitter.tokenize(self.tweet):
			if len(sentence) == 1 and sentence[0] == '*': # remove the * we added previously
 				continue

 			if sentence[-1] == '*':
 				sentence = sentence[:-1]

 			
			token_tweet.append([ (t[0] + "/" + t[1]) for t in nltk.pos_tag(tbwt.tokenize(sentence))])

		return token_tweet

	def __str__(self):

		output = ''

		for sentence in self.tweet_sentence:
			output = output + ' '.join(sentence) + '\n'
		
		return "<A={}>\n".format(self.tweet_data[0]) + output[:-1]



if len(sys.argv) != 3:
	raise Exception("Usage: python preprocessing384.py input_csv_file output_csv_file")

with open(sys.argv[1]) as rfile:
	list_output = []
	for tweet in rfile:
		try:
			tp = TwitterParser(tweet)
		except:
			continue
		else:
			list_output.append(str(tp))	# append TwitterParser instance as a string, not the instance itself

wfile = open(sys.argv[2], 'w')
wfile.write('\n'.join(list_output))
wfile.close()